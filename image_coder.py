from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from subprocess import call
import os

class ImageCoder:
    def __init__(self, filename):
        self.filename = filename
        self.image = Image.open(filename)
        self.height = self.image.size[0]
        self.width = self.image.size[1]

    def encrypt(self, keyFile, round):
        print('Encrypt')
        key = 5342
        data = [0, key]

        rgb = bytearray(np.asarray(self.image).flatten())
        
        for i in range(0, len(rgb) - 4, 4):
            data[0] = self.bytesToInteger(rgb, i)

        os.system("java -jar ima.jar e " + self.filename + " " + str(round))

    def decrypt(self, keyFile, round):
        print('Decrypt')
        key = 5342
        data = [0, key]

        rgb = bytearray(np.asarray(self.image).flatten())
        
        for i in range(0, len(rgb) - 4, 4):
            data[0] = self.bytesToInteger(rgb, i)

        os.system("java -jar ima.jar d res/r" + str(round) + "/noise.bmp " + str(round))


    def bytesToInteger(self, source, offset):
        result = int(0)
        end = offset + 4
        if end > len(source):
            end = len(source)

        for i in range(offset, end):
            result = int(result << 8)
            result |= int((source[i] & 255))

        return result