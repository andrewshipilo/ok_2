import random
from util import *
import matplotlib.pyplot as plt



def permute(bitstr, controlstr, reverse=False):
    bitstr = "".join(["0"] * (16 - len(bitstr))) + bitstr
    bitstr = bitstr.replace("b", "")
    U = [(0, 1), (2, 3), (4, 5), (6, 7), (8, 9), (10, 11), (12, 13), (14, 15), (1, 2), (3, 4), (5, 6), (7, 8), (9, 10), (11, 12), (13, 14), (0, 3), (4, 7), (8, 11), (12, 15), (3, 4), (11, 12), (2, 5), (10, 13), (1, 6), (9, 14), (0, 7), (8, 15), (7, 8), (6, 9), (5, 10), (4, 11), (3, 12), (2, 13), (1, 14), (0, 15)]
    if reverse:
        U = U[::-1]
        controlstr = controlstr[::-1]

    # print( len(controlstr))
    # print(len(U))

    res = [None] * 16
    for j in range(len(bitstr)):
        #print("bit number", j)
        currentidx = j
        for i in range(len(U)):
            if currentidx in U[i] and controlstr[i] == "1":
                #print("\tfrom", currentidx, end= " ")
                currentidx = U[i][0] if currentidx == U[i][1] else U[i][1]
                #print("to", currentidx, "(" + str(i) + ")")
        res[currentidx] = bitstr[j]

    #print([1 if i else 0 for i in res])
    return "".join(res)


def xor_bitstr(bitstr1, bitstr2):
    bitstr1 = bitstr1.replace("b", "")
    bitstr2 = bitstr2.replace("b", "")
    return "".join([str(int(bitstr1[i]) ^ int(bitstr2[i])) for i in range(len(bitstr1))])


def expand(bitstr):
    split = [bitstr[i:i + 4] for i in range(0, len(bitstr), 4)]
    res = [split[1][0] + split[0] + split[-1][-1]]
    end = split[0] + split[0][0] + split[-1] + split[-2][-1]

    for i in range(1, len(split) - 1):
        res.append(split[i + 1][0] + split[i] + split[i - 1][-1])

    res.append(split[0])
    res.append(split[2][1:])


    res.append(end)


    return "".join(res)


def twos_comp(val):
    bits = 8
    """compute the 2's complement of int value val"""
    if (val & (1 << (bits - 1))) != 0: # if sign bit is set e.g., 8bit: 128-255
        val = val - (1 << bits)        # compute negative value
    return val                         # return positive value as is



def encrypt_block(bitstr, key):
    if len(bitstr) != 32 or len(key) != 32:
        print("wrong len")
        return ""

    k1 = key[:len(key) // 2]
    k2 = key[len(key) // 2:]

    L = bitstr[:len(bitstr) // 2]
    R = bitstr[len(bitstr) // 2:]

    L = xor_bitstr(L, k1)

    R = bin(int(R, 2) + int(k2, 2))[-len(L):]
    R = R.replace("b", "")
    R = "".join(["0"] * (16 - len(R))) + R

    expandedR = expand(R)
    L = permute(L, expandedR)


    L = int(L, 2) + int(R, 2)
    L = bin(L)[-len(k1):]
    L = L.replace("b", "")
    L = "".join(["0"] * (16 - len(L))) + L


    expandedL = expand(L)
    R = permute(R, expandedL)
    R = xor_bitstr(L, R)
    return L + R


def decrypt_block(bitstr, key):
    if len(bitstr) != 32 or len(key) != 32:
        print("wrong len")
        return ""

    k1 = key[:len(key) // 2]
    k2 = key[len(key) // 2:]

    L = bitstr[:len(bitstr) // 2]
    R = bitstr[len(bitstr) // 2:]

    permutedR = xor_bitstr(L, R)
    expandedL = expand(L)
    permutedR = permute(permutedR, expandedL, True)

    permutedL = (int(L, 2) + (2**16 - int(permutedR, 2))) % 2**16
    permutedL = bin(permutedL)[-len(L):]
    permutedL = permutedL.replace("b", "")
    permutedL = "".join(["0"] * (16 - len(permutedL))) + permutedL

    expandedR = expand(permutedR)
    permutedL = permutedL.replace("b", "")
    permutedL = permute(permutedL, expandedR, True)

    R = (int(permutedR, 2) + (2**16 - int(k2, 2))) % 2**16
    R = bin(R)[-len(L):]
    R = R.replace("b", "")
    R = "".join(["0"] * (16 - len(R))) + R
    # print("\t", R)
    permutedL = permutedL.replace("b", "")
    L = xor_bitstr(permutedL, k1)

    return L + R


def pad(str):
    tmp = "".join(["0"]*(8 - len(str))) + str
    return tmp



if __name__ == "__main__":
    rounds = 5

    keys = ["".join(["1" if random.random() < 0.5 else "0" for i in range(32)]) for j in range(rounds)]
    keysOfDecryptions = keys[::-1]
    header, lena = readBmp("lena.bmp")
    data = lena[..., 0]
    for i in range(0, data.shape[0], 2):
        for j in range(0, data.shape[1], 2):
            bitstr = pad(bin(data[i][j]).replace("b", "")[-8:]) + pad(bin(data[i][j + 1]).replace("b", "")[-8:])
            bitstr += pad(bin(data[i+1][j]).replace("b", "")[-8:]) + pad(bin(data[i+1][j + 1]).replace("b", "")[-8:])
            if len(bitstr) != 32:
                print("invalid size")
            resOfEncrypted = bitstr
            for key in keys:
                resOfEncrypted = encrypt_block(resOfEncrypted, key)

            data[i][j] = int(resOfEncrypted[:8], 2)
            data[i][j + 1] = int(resOfEncrypted[8:16], 2)
            data[i+1][j] = int(resOfEncrypted[16:24], 2)
            data[i + 1][j + 1] = int(resOfEncrypted[24:], 2)
    lena[..., 0] = lena[..., 1] = lena[..., 2] = data
    writeBmp("encrypted" + str(rounds) + ".bmp", header, lena)

    data = data.flatten()
    d = []
    d.append(correlation(data[:], data[:]))
    for i in range(1, 1000):

        d.append(correlation(data[i:], data[:-i]))
    plt.plot(range(1000), d)
    #plt.show()
    plt.savefig("correl" + str(rounds) + ".png")
    plt.clf()
    data1 = data.flatten()[:int(len(data.flatten()) / 4)]
    plt.plot(data1[:-1:], data1[1:], 'bo', markersize=0.1)
    #plt.show()
    plt.savefig("test" + str(rounds) + ".png")

    #
    # header, lena = readBmp("encrypted" + str(rounds) + ".bmp")
    # data = lena[..., 0]
    # for i in range(0, data.shape[0], 2):
    #     for j in range(0, data.shape[1], 2):
    #         bitstr = pad(bin(data[i][j]).replace("b", "")[-8:]) + pad(bin(data[i][j + 1]).replace("b", "")[-8:])
    #         bitstr += pad(bin(data[i+1][j]).replace("b", "")[-8:]) + pad(bin(data[i+1][j + 1]).replace("b", "")[-8:])
    #         if len(bitstr) != 32:
    #             print("invalid size")
    #         resOfDecrypted = bitstr
    #         for key in keysOfDecryptions:
    #             resOfDecrypted = decrypt_block(resOfDecrypted, key)
    #
    #         data[i][j] = int(resOfDecrypted[:8], 2)
    #         data[i][j + 1] = int(resOfDecrypted[8:16], 2)
    #         data[i+1][j] = int(resOfDecrypted[16:24], 2)
    #         data[i + 1][j + 1] = int(resOfDecrypted[24:], 2)
    # lena[..., 0] = lena[..., 1] = lena[..., 2] = data
    # writeBmp("decrypted" + str(rounds) + ".bmp", header, lena)