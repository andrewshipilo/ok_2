from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

class ImageTester:
    def __init__(self, filename):
        self.image = Image.open(filename)
        self.height = self.image.size[0]
        self.width = self.image.size[1]

    def autocorrelation(self, path, start, step, stop):
        autocorrelations = [[] for i in range(3)]
        tau = [i for i in range(start, stop, step)]

        for t in tau:
            tmp = self.getAutocorrelations(t)
            for j in range(len(tmp)):
                autocorrelations[j].append(tmp[j])

        plt.figure()
        plt.plot(tau, autocorrelations[0], label='Red', color='red', )
        plt.plot(tau, autocorrelations[1], label='Green', color='green')
        plt.plot(tau, autocorrelations[2], label='Blue', color='blue')
        plt.title('Autocorrelation')
        plt.legend()
        plt.savefig(path + '/autocorr.png')

    def lattice(self, path):
        x = []
        y = []
        rgb = np.asarray(self.image).flatten()

        for i in range(0, len(rgb) - 3, 3):
            x.append(rgb[i])
            y.append(rgb[i + 3])

        plt.figure()
        plt.scatter(x, y, alpha=0.025, linewidths=0)
        plt.savefig(path + '/lattice.png')


    def getAutocorrelations(self, tau):
        size = self.height * self.width

        autocorrelations = [i for i in range(3)]
        averages = self.getAverages(0, size - tau)
        tauAvgs = self.getAverages(tau, size)
        stdDev = self.getStandartDeviation(0, size - tau)
        tauDev = self.getStandartDeviation(tau, size)
        
        for i in range(size - tau):
            pixel = self.getPixel(i)
            tauPixel = self.getPixel(i + tau)

            for j in range(len(pixel)):
                autocorrelations[j] += (pixel[j] - averages[j]) * (tauPixel[j] - tauAvgs[j])

        for i in range(len(autocorrelations)):
            autocorrelations[i] /= size * stdDev[i]**2

        return autocorrelations

    def getPixel(self, index):
        x = index / self.height
        y = index % self.width

        return self.image.getpixel((x, y))

    def getStandartDeviation(self, start, end): 
        r, g, b = self.image.split()
        return [
            np.std(np.asarray(r).flatten()[start:end]),
            np.std(np.asarray(g).flatten()[start:end]),
            np.std(np.asarray(b).flatten()[start:end])
         ]

    def getAverages(self, start, end):   
        r, g, b = self.image.split()
        return [
            np.mean(np.asarray(r).flatten()[start:end]),
            np.mean(np.asarray(g).flatten()[start:end]),
            np.mean(np.asarray(b).flatten()[start:end])
         ]

