import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from collections import defaultdict
import struct


def sat(val, minV, maxV):
    if val < minV:
        val = minV
    elif val > maxV:
        val = maxV

    return val


# def psnr(data, newdata):
#     data = np.array(data)
#     newdata = np.array(newdata)
#
#     top = len(data) * (255) ** 2
#     botR = sum((data[:, 0] - newdata[..., 0]) ** 2)
#     # botR = sum((data[i][0] - newdata[i][0])**2 for i in range(0, len(data)))
#     botG = sum((data[i][1] - newdata[i][1]) ** 2 for i in range(0, len(data)))
#     botB = sum((data[i][2] - newdata[i][2]) ** 2 for i in range(0, len(data)))
#
#     return [10 * math.log10(top / botR), 10 * math.log10(top / botG), 10 * math.log10(top / botB)]


def correlation(a, b):
    exp_val_a = np.average(a)
    exp_val_b = np.average(b)

    res_top = np.sum((a - exp_val_a) * (b - exp_val_b))
    res_bot = np.sum(((a - exp_val_a) ** 2)) * np.sum((b - exp_val_b) ** 2)

    return res_top / np.sqrt(res_bot)


def auto_correlation(array, y, t):
    if y == 0:
        a = array
        b = array
    else:
        if y > 0:
            a = array[y:, ...]
            b = array[:-y, ...]
        else:
            a = array[:y, ...]
            b = array[-y:, ...]
    crl = []
    # crl = [(correlation(a[..., -i:], b[..., :i])) for i in t]
    for i in t:
        if i != 0:
            crl.append((correlation(a[..., :-i], b[..., i:])))
        else:
            crl.append((correlation(a, b)))
    return crl


def into_ycbcr(value):
    value = value.astype(np.int32)
    transform_matrix = np.array([[.299, .587, .114], [-.1687, -.3313, .5], [.5, -.4187, -.0813]])
    ycbcr = value.dot(transform_matrix.T)
    ycbcr[:, :, [1, 2]] += 128
    np.putmask(ycbcr, ycbcr > 256, 255)

    return np.uint8(ycbcr)


def into_rgb(value):
    xform = np.array([[1, 0, 1.402], [1, -0.34414, -.71414], [1, 1.772, 0]])
    rgb = value.astype(np.float)
    rgb[..., [1, 2]] -= 128
    # print(rgb.min())
    # print(rgb.max())
    rgb = rgb.dot(xform.T)
    np.putmask(rgb, rgb < 0, 0)
    np.putmask(rgb, rgb > 255, 255)
    # print(rgb.min())
    # print(rgb.max())
    return np.uint8(rgb)


def psnr(a, b):
    h = a.shape[0]
    w = a.shape[1]
    bottom = np.sum((a - b) ** 2)
    top = w * h * ((2 ** 8) - 1) ** 2
    return 10 * np.log10(top / bottom)


def autcorrel_plot(array, title):
    t = (range(0, array.shape[1] // 4, 5))
    corg = auto_correlation(array, 5, t)
    corb = auto_correlation(array, -5, t)
    corr = auto_correlation(array, 0, t)
    cory = auto_correlation(array, 10, t)
    corblacl = auto_correlation(array, -10, t)
    plt.title(title)
    l1 = mpatches.Patch(color='g', label='y = 5')
    l2 = mpatches.Patch(color='b', label='y = -5')
    l3 = mpatches.Patch(color='r', label='y = 0')
    l4 = mpatches.Patch(color='y', label='y = 10')
    l5 = mpatches.Patch(color='black', label='y = -10')
    plt.legend(handles=[l1, l2, l3, l4, l5])
    plt.plot(t, corg, color='g')
    plt.plot(t, corb, color='b')
    plt.plot(t, corr, color='r')
    plt.plot(t, cory, color='y')
    plt.plot(t, corblacl, color='black')
    # plt.savefig(f"auto correlation {title}.png")
    plt.show()
    plt.clf()


def decimation_exclusion(value, n):
    return np.copy(value[::n, ::n])


def decimation_average(value, n):
    y = value.shape[0]
    x = value.shape[1]
    res = np.zeros((y, x), 'uint8')
    for i in range(0, y, n):
        for j in range(0, x, n):
            w = 0
            avg = 0
            if i + 1 < y:
                w += 1
                avg += value[i + 1][j]
            if i - 1 > 0:
                w += 1
                avg += value[i - 1][j]
            if j + 1 < x:
                w += 1
                avg += value[i][j + 1]
            if j - 1 > 0:
                w += 1
                avg += value[i][j - 1]
            res[i][j] = avg // w
    return decimation_exclusion(res, n)


def decimation_recover(value, n):
    y = value.shape[0]
    x = value.shape[1]
    res = np.zeros((y * n, x))

    for i in range(0, y):
        j = i * n
        for z in range(0, n):
            res[j + z, ...] = value[i, ...]

    res = np.hstack((res, np.zeros((res.shape[0], x * (n - 1)), dtype='uint8')))
    value = np.copy(res)

    for i in range(0, x):
        j = i * n

        for z in range(0, n):
            res[..., j + z] = value[..., i]

    return res


def decimation_test(rgb, n, header):
    y = rgb.shape[0]
    x = rgb.shape[1]
    y_cb_cr = into_ycbcr(rgb)
    resY = decimation_exclusion(y_cb_cr[..., 0], n)
    resCB = decimation_exclusion(y_cb_cr[..., 1], n)
    resCR = decimation_exclusion(y_cb_cr[..., 2], n)
    recovY = decimation_recover(resY, n)
    recovCB = decimation_recover(resCB, n)
    recobCR = decimation_recover(resCR, n)
    rgb_recovered1 = np.zeros((y, x, 3), 'uint8')
    # rgb_recovered1[..., 0] = y_cb_cr[..., 0]
    rgb_recovered1[..., 0] = recovY
    rgb_recovered1[..., 1] = recovCB
    rgb_recovered1[..., 2] = recobCR

    print(f"Exclusion decimation for n={n}:")
    print("psnr cb", psnr(y_cb_cr[..., 1], recovCB))
    print("psnr cr", psnr(y_cb_cr[..., 2], recobCR))
    rgb_recovered1 = into_rgb(rgb_recovered1)
    writeBmp(f"{n}-exclusion-decimation.bmp", header, rgb_recovered1)

    print("psnr blue", psnr(rgb[..., 0], rgb_recovered1[..., 0]))

    print("psnr green", psnr(rgb[..., 1], rgb_recovered1[..., 1]))
    print("psnr red", psnr(rgb[..., 2], rgb_recovered1[..., 2]))

    print(f"\nAverage decimation for n={n}:")
    resG = decimation_average(y_cb_cr[..., 1], n)
    resR = decimation_average(y_cb_cr[..., 2], n)
    resG2 = decimation_recover(resG, n)
    resR2 = decimation_recover(resR, n)
    rgb_recovered2 = np.zeros((y, x, 3), 'uint8')
    rgb_recovered2[..., 0] = y_cb_cr[..., 0]
    rgb_recovered2[..., 1] = resG2
    rgb_recovered2[..., 2] = resR2
    print("psnr cb", psnr(y_cb_cr[..., 1], resG2))
    print("psnr cr", psnr(y_cb_cr[..., 2], resR2))
    rgb_recovered2 = into_rgb(rgb_recovered2)
    writeBmp(f"{n}-average-decimation.bmp", header, rgb_recovered2)
    print("psnr blue", psnr(rgb[..., 0], rgb_recovered2[..., 0]))
    print("psnr green", psnr(rgb[..., 1], rgb_recovered2[..., 1]))
    print("psnr red", psnr(rgb[..., 2], rgb_recovered2[..., 2]))


def hist_entropy(rgb):
    y_cb_cr = into_ycbcr(rgb)
    x = range(0, 255)
    freq_blue = defaultdict(int)
    freq_green = defaultdict(int)
    freq_red = defaultdict(int)
    freq_cb = defaultdict(int)
    freq_cr = defaultdict(int)
    freq_y = defaultdict(int)
    for b, g, r, cb, cr, y in zip(rgb[..., 0].flatten(), rgb[..., 1].flatten(), rgb[..., 2].flatten(),
                                  y_cb_cr[..., 1].flatten(), y_cb_cr[..., 2].flatten(), y_cb_cr[..., 0].flatten()):
        freq_blue[b] += 1
        freq_green[g] += 1
        freq_red[r] += 1
        freq_cb[cb] += 1
        freq_cr[cr] += 1
        freq_y[y] += 1
    b = []
    g = []
    r = []
    cb = []
    cr = []
    y = []
    for i in x:
        b.append(freq_blue[i])
        g.append(freq_green[i])
        r.append(freq_red[i])
        cb.append(freq_cb[i])
        cr.append(freq_cr[i])
        y.append(freq_y[i])

    size = rgb[..., 0].size

    for i in range(0, 255):
        b[i] = b[i] / size
        g[i] = g[i] / size
        r[i] = r[i] / size
        cb[i] = cb[i] / size
        cr[i] = cr[i] / size
        y[i] = y[i] / size

    plt.bar(x, b)
    plt.title("blue")
    # plt.savefig("hist for blue.png")
    plt.show()
    plt.clf()

    plt.bar(x, g)
    plt.title("green")
    # plt.savefig("hist for green.png")
    plt.show()
    plt.clf()

    plt.bar(x, r)
    plt.title("red")
    # plt.savefig("hist for red.png")
    plt.show()
    plt.clf()

    plt.bar(x, cb)
    plt.title("cb")
    # plt.savefig("hist for cb.png")
    plt.show()
    plt.clf()

    plt.bar(x, cr)
    plt.title("cr")
    # plt.savefig("hist for cr.png")
    plt.show()
    plt.clf()

    plt.bar(x, y)
    plt.title("y")
    # plt.savefig("hist for cr.png")
    plt.show()
    plt.clf()

    entropy_b = -np.sum(b * np.ma.log2(b).filled(0))
    entropy_g = -np.sum(g * np.ma.log2(g).filled(0))
    entropy_r = -np.sum(r * np.ma.log2(r).filled(0))
    entropy_cb = -np.sum(cb * np.ma.log2(cb).filled(0))
    entropy_cr = -np.sum(cr * np.ma.log2(cr).filled(0))
    entropy_y = -np.sum(y * np.ma.log2(y).filled(0))
    print("\n")
    print("entropy b ", entropy_b)
    print("entropy g ", entropy_g)
    print("entropy r ", entropy_r)
    print("entropy cb ", entropy_cb)
    print("entropy cr ", entropy_cr)
    print("entropy y ", entropy_y)


def readBmp(path):
    bmp = open(path, "rb")
    header = {'typeOfPic': bmp.read(2).decode(),
              'size': struct.unpack('I', bmp.read(4)),
              'res1': struct.unpack('H', bmp.read(2)),
              'res2': struct.unpack('H', bmp.read(2)),
              'offset': struct.unpack('I', bmp.read(4)),
              'headerSize': struct.unpack('I', bmp.read(4)),
              'width': struct.unpack('I', bmp.read(4)),
              'height': struct.unpack('I', bmp.read(4)),
              'ColourPlanes': struct.unpack('H', bmp.read(2)),
              'bitsPerPixel': struct.unpack('H', bmp.read(2)),
              'compressionMethod': struct.unpack('I', bmp.read(4)),
              'rawImageSize': struct.unpack('I', bmp.read(4)),
              'horizontalResolution': struct.unpack('I', bmp.read(4)),
              'verticalResolution': struct.unpack('I', bmp.read(4)),
              'numberofColours': struct.unpack('I', bmp.read(4)),
              'importantColours': struct.unpack('I', bmp.read(4))
              }

    ar = np.fromfile(bmp, dtype='uint8')
    ar = ar.reshape(header["width"][0], header["height"][0], header["bitsPerPixel"][0] // 8)
    ar = ar[::-1, :, ::-1]
    # #ar = np.array(ba)
    # size = ar.size
    # padding = size - header['rawImageSize'][0]
    # ar = np.delete(ar, range(size - 1 - padding, size - 1))
    # width = header['width'][0]
    # padding_size = (width * 3) % 4
    # row_len = padding_size + width * 3
    # ar = ar.reshape(-1, row_len)
    # ar = np.delete(ar, range(row_len - padding_size - 1, row_len - 1), 1)
    # ar = ar.reshape(-1, row_len // 3, 3)
    return header, ar


def writeBmp(path, header, rgb_array):
    rgb_array = rgb_array[::-1, :, ::-1]
    packed_header = {
        'typeOfPic': header['typeOfPic'].encode(),
        'size': struct.pack('I', header['size'][0]),
        'res1': struct.pack('H', header['res1'][0]),
        'res2': struct.pack('H', header['res2'][0]),
        'offset': struct.pack('I', header['offset'][0]),
        'headerSize': struct.pack('I', header['headerSize'][0]),
        'width': struct.pack('I', header['width'][0]),
        'height': struct.pack('I', header['height'][0]),
        'ColourPlanes': struct.pack('H', header['ColourPlanes'][0]),
        'bitsPerPixel': struct.pack('H', header['bitsPerPixel'][0]),
        'compressionMethod': struct.pack('I', header['compressionMethod'][0]),
        'rawImageSize': struct.pack('I', header['rawImageSize'][0]),
        'horizontalResolution': struct.pack('I', header['horizontalResolution'][0]),
        'verticalResolution': struct.pack('I', header['verticalResolution'][0]),
        'numberofColours': struct.pack('I', header['numberofColours'][0]),
        'importantColours': struct.pack('I', header['importantColours'][0])
    }
    # padding_len = rgb_array.shape[1] % 4
    rgb_array = rgb_array.reshape(rgb_array.shape[0], rgb_array.shape[1] * 3)
    # rgb_array = np.hstack((rgb_array, np.zeros((rgb_array.shape[0], padding_len), dtype='uint8')))

    out = open(path, "wb")
    for key, val in packed_header.items():
        out.write(val)
    out.write(rgb_array.tobytes())


def impulse_noise(val, pa, pb, maxval=255, minval=0):
    rnd = np.random.random()
    if rnd < pa:
        return minval
    elif rnd < pa + pb:
        return maxval
    else:
        return val


# def map_angle(x):
#     if 90 < x <= 0:
#         return

# test = np.empty((8, 8))
# test[...] = 0
#
# T = np.empty((8, 8))
# for f in range(8):
#     for t in range(8):
#         T[f, t] = np.cos(((2*t + 1) * np.pi * f) / 16)
#
#         if f == 0:
#             T[f, t] *= np.sqrt(1/8)
#         else:
# #             T[f, t] *= np.sqrt(1/4)
# # print(T)
# # print((np.around(((T.dot(test)).dot(T.transpose()))).astype(int)))

def zigzag():
    # for i in range(32):
    #     print(i)
    #     print(i.bit_length())
    #     print(bin(i))
    #     j = -i
    #     print(j.bit_length())
    #     print(bin(j))
    st = np.array([0, 1])
    t = []
    updown = True
    while st[0] < 7 or st[1] < 7:
        t.append(st.copy())
        # print(st)
        if (updown):
            st[0] = st[0] + 1
            st[1] = st[1] - 1
        else:
            st[0] = st[0] - 1
            st[1] = st[1] + 1

        if updown:
            if st[0] == 7:
                t.append(st.copy())
                # print(st)
                st[1] += 1
                updown = False
            elif st[1] == 0:
                t.append(st.copy())
                # print(st)
                st[0] += 1
                updown = False
        else:
            if st[0] == 0:
                t.append(st.copy())
                # print(st)
                st[1] += 1
                updown = True
            elif st[1] == 7:
                t.append(st.copy())
                # print(st)
                st[0] += 1
                updown = True

    t.append(np.array([7, 7]))
    return t

def entropy(sample):
    freqsDeltas = {}

    for i in sample:
        if i in freqsDeltas:
            freqsDeltas[i] += 1
        else:
            freqsDeltas[i] = 1
    entropyDC = 0

    for val in freqsDeltas.values():
        prob = val / sum(freqsDeltas.values())
        entropyDC -= prob * np.log2(prob)
    return entropyDC

# sprint(bin(5))
# print(len(bin(5)) - 2)