from image_tester import ImageTester
from image_coder import ImageCoder


def testImage(filePath):
    tester = ImageTester(filePath)
    tester.autocorrelation('res', 0, 64, 768)
    tester.lattice('res')

def testEncrypt(filePath):
    for rnd in range(1, 2): 
        
        coder = ImageCoder(filePath)
        coder.encrypt('key', rnd)

        tester = ImageTester("res/r" + str(rnd) + "/noise.bmp")
        tester.autocorrelation('res/r' + str(rnd), 0, 64, 768)
        tester.lattice('res/r' + str(rnd))

        coder.decrypt('key', rnd)

testEncrypt('lenna.bmp')
#testImage('lena.bmp')